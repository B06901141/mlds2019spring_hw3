import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from const import *
from collections import Counter
from operator import itemgetter

def confusion_matrix():
    result = np.load('100predict.npy')
    r = len(tag_to_idx)
    confusion_map = np.zeros((r, r))
    tag_ctr = [Counter(clf) for clf in result]

    for i, ctr in enumerate(tag_ctr):
        for cl, num in ctr.items():
            confusion_map[i][cl] = num
    confusion_map /= 100

    idx_to_tag = dict()
    for tag, i in tag_to_idx.items():
        idx_to_tag[i] = tag

    tags = [idx_to_tag[i] for i in range(130)]

    '''
    plt.figure(figsize=(r, r))
    plt.title('Confusion Matrix')
    plt.imshow(confusion_map, interpolation='nearest', cmap=cm.get_cmap('RdYlBu', 5))
    plt.colorbar()
    plt.xticks(np.arange(r), tags)
    plt.yticks(np.arange(r), tags)
    plt.tight_layout()
    plt.savefig('graphic/confusion_matrix.png')
    '''

    accu_acc = 0.0
    for i in range(130):
        accu_acc += confusion_map[i][i]
    print(accu_acc / 130)

def hair_confusion_matrix():
    result = np.load('100predict.npy')

    idx_to_tag = dict()
    for tag, i in tag_to_idx.items():
        idx_to_tag[i] = tag

    hc = sorted(set([tag.split(' ')[0] for tag in tag_to_idx.keys()]))
    hc_dict = dict()
    num_hc = len(hc)

    for i, c in enumerate(hc):
        hc_dict[c] = i

    def tag2hc(i):
        return hc_dict[idx_to_tag[i].split(' ')[0]]

    project_ctr = [Counter([idx for idx in map(tag2hc, dist)]) for dist in result]
    confusion_map = np.zeros((num_hc, num_hc))

    for i, ctr in enumerate(project_ctr):
        hid = tag2hc(i)
        for cl, num in ctr.items():
            confusion_map[hid][cl] += num

    confusion_map /= np.sum(confusion_map, axis=1)

    plt.figure(figsize=(num_hc, num_hc))
    plt.title("Hair's Confusion Matrix")

    plt.imshow(confusion_map, interpolation='nearest', cmap=cm.get_cmap('RdYlBu', 10))
    plt.clim(0, 1)
    plt.colorbar()
    plt.xticks(np.arange(num_hc), list(hc))
    plt.yticks(np.arange(num_hc), list(hc))
    plt.tight_layout()
    plt.savefig('graphic/hair_confusion_matrix.png')

def eyes_confusion_matrix():
    result = np.load('100predict.npy')

    idx_to_tag = dict()
    for tag, i in tag_to_idx.items():
        idx_to_tag[i] = tag

    ec = sorted(set([tag.split(' ')[2] for tag in tag_to_idx.keys()]))
    ec_dict = dict()
    num_ec = len(ec)

    for i, c in enumerate(ec):
        ec_dict[c] = i

    def tag2ec(i):
        return ec_dict[idx_to_tag[i].split(' ')[2]]

    project_ctr = [Counter([idx for idx in map(tag2ec, dist)]) for dist in result]
    confusion_map = np.zeros((num_ec, num_ec))

    for i, ctr in enumerate(project_ctr):
        eid = tag2ec(i)
        for cl, num in ctr.items():
            confusion_map[eid][cl] += num

    confusion_map /= np.sum(confusion_map, axis=1)

    plt.figure(figsize=(num_ec, num_ec))
    plt.title("Eyes' Confusion Matrix")

    plt.imshow(confusion_map, interpolation='nearest', cmap=cm.get_cmap('RdYlBu', 10))
    plt.clim(0, 0.5)
    plt.colorbar()
    plt.xticks(np.arange(num_ec), list(ec))
    plt.yticks(np.arange(num_ec), list(ec))
    plt.tight_layout()
    plt.savefig('graphic/eyes_confusion_matrix_0.5.png')

def baseline_matrix():
    with open('output.log', 'r') as fin:
        score = fin.read().split('\n')
        score = score[:len(score) - 1]
        score = [int(score[2 * i].split(' ')[1]) for i in range(int(len(score) / 2))]
    fl = os.listdir('30000')
    tags = [f.split('.')[0] for f in fl]
    hair_color = [tag.split(' ')[0] for tag in tags]
    eye_color = [tag.split(' ')[2] for tag in tags]

    hc = sorted(set(hair_color))
    hc_dict = dict()
    for i, c in enumerate(hc):
        hc_dict[c] = i

    ec = sorted(set(eye_color))
    ec_dict = dict()
    for i, c in enumerate(ec):
        ec_dict[c] = i

    num_hc = len(hc)
    num_ec = len(ec)

    pass_map = np.zeros(shape= (num_hc, num_ec))

    for i, s in enumerate(score):
        pass_map[hc_dict[hair_color[i]]][ec_dict[eye_color[i]]] = s

    plt.figure(figsize=(num_hc, num_ec))
    plt.title('Baseline Matrix')
    plt.imshow(pass_map, interpolation='nearest', cmap=cm.get_cmap('RdYlBu', 5))
    plt.colorbar()
    plt.xticks(np.arange(num_ec), list(ec))
    plt.yticks(np.arange(num_hc), list(hc))
    plt.tight_layout()
    plt.xlabel("Eyes' color")
    plt.ylabel("Hair's color")
    plt.savefig('graphic/baseline_matrix.png')

if __name__ == '__main__':
    baseline_matrix()
    confusion_matrix()
    hair_confusion_matrix()
    eyes_confusion_matrix()