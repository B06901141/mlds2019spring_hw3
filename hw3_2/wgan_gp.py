from functools import partial

import keras.backend as K
import matplotlib.pyplot as plt
import numpy as np
from keras.layers import (Activation, BatchNormalization, Dense, Dropout,
                          Flatten, Input, Reshape, ZeroPadding2D, concatenate)
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Conv2D, Conv2DTranspose, UpSampling2D
from keras.layers.merge import _Merge
from keras import layers
from keras.models import Model, Sequential, load_model
from keras.optimizers import RMSprop
from keras.utils import to_categorical
from keras.layers import GaussianNoise

from time import time
import pandas as pd

from collections import Counter
import pickle


class Pair:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def get_images_tags(filename):
    def mapdata(tag):
        return tag_to_idx[tag]
    data = pd.read_csv(filename)
    tags = data[data.columns[1]]
    tags = [d for d in map(mapdata, data[data.columns[1]])]
    return tags

def read_preprocessimg():
    '''
    make images in (-1, 1)
    '''
    tags = get_images_tags('%stags.csv' % home)
    images = pickle.load(open('%simages.pkl' % home, 'rb'))
    images = (images - 127.5) / 127.5
    # paired = [(images[i], tags[i])  for i in range(len(images))]
    # np.random.shuffle(paired)
    
    data = []

    tag_count = Counter(tags)
    start = 0
    for i, num in tag_count.items():
        same_tag_image = images[start: start + num]
        # processed_tag = np.random.normal(0, 1, size=(num, 130))
        # for i in range(num):
        #     processed_tag[i][tags[start + i]] = 10
        data.append(Pair(same_tag_image, to_categorical(tags[start: start + num], num_classes=130)))
        start += num
    np.random.shuffle(tags)
    return data, tags

class RandomWeightedAverage(_Merge):
    """Provides a (random) weighted average between real and generated image samples"""
    def _merge_function(self, inputs):
        alpha = K.random_uniform((1, 1, 1, 1))
        return (alpha * inputs[0]) + ((1 - alpha) * inputs[1])

class WGANGP():
    def __init__(self, load = False):
        self.img_rows = 64
        self.img_cols = 64
        self.channels = 3
        self.img_shape = (self.img_rows, self.img_cols, self.channels)
        self.latent_dim = 128
        self.text_dim = 130

        # Following parameter and optimizer set as recommended in paper
        self.n_critic = 5
        optimizer = RMSprop(lr=0.00005)

        # Build the generator and critic
        if load:
            self.generator = load_model('%smodel/generator.hdf5' % home)
            self.critic = load_model('%smodel/critic.hdf5' % home)

        else:
            self.generator = self.build_generator()
            self.critic = self.build_critic()

        #-------------------------------
        # Construct Computational Graph
        #       for the Critic
        #-------------------------------

        # Freeze generator's layers while training critic
        self.generator.trainable = False

        # Image input (real sample)
        real_img = Input(shape=(self.img_shape))
        real_tag = Input(shape=(self.text_dim,))
        fake_tag = Input(shape=(self.text_dim,))


        # Noise input is add in text
        # noise = Input(shape=(self.latent_dim,))
        # Generate image based of noise (fake sample)
        fake_img = self.generator(real_tag)

        # Discriminator determines validity of the real and fake images
        fake = self.critic([fake_img, real_tag])
        valid = self.critic([real_img, real_tag])
        false_tag = self.critic([real_img, fake_tag])

        # Construct weighted average between real and fake images
        
        interpolated_img = RandomWeightedAverage()([real_img, fake_img])
        # Determine validity of weighted sample
        validity_interpolated = self.critic([interpolated_img, real_tag])

        # Use Python partial to provide loss function with additional
        # 'averaged_samples' argument
        partial_gp_loss = partial(self.gradient_penalty_loss,
                          averaged_samples=interpolated_img)
        partial_gp_loss.__name__ = 'gradient_penalty' # Keras requires function names

        self.critic_model = Model(inputs=[real_img, real_tag, fake_tag],
                            outputs=[valid, fake, false_tag, validity_interpolated])
        self.critic_model.compile(loss=[self.wasserstein_loss,
                                              self.wasserstein_loss,
                                              self.wasserstein_loss,
                                              partial_gp_loss],
                                        optimizer=optimizer,
                                        loss_weights=[1, 1, 1, 1])
        #-------------------------------
        # Construct Computational Graph
        #         for Generator
        #-------------------------------

        # For the generator we freeze the critic's layers
        self.critic.trainable = False
        self.generator.trainable = True

        # Sampled noise for input to generator
        noisy_tag = Input(shape=(self.text_dim,))
        # Generate images based of noise
        img = self.generator(noisy_tag)
        # Discriminator determines validity
        valid = self.critic([img, noisy_tag])
        # Defines generator model
        self.generator_model = Model(noisy_tag, valid)
        self.generator_model.compile(loss=self.wasserstein_loss, optimizer=optimizer)


    def gradient_penalty_loss(self, y_true, y_pred, averaged_samples):
        """
        Computes gradient penalty based on prediction and weighted real / fake samples
        """
        gradients = K.gradients(y_pred, averaged_samples)[0]
        # compute the euclidean norm by squaring ...
        gradients_sqr = K.square(gradients)
        #   ... summing over the rows ...
        gradients_sqr_sum = K.sum(gradients_sqr,
                                  axis=np.arange(1, len(gradients_sqr.shape)))
        #   ... and sqrt
        gradient_l2_norm = K.sqrt(gradients_sqr_sum)
        # compute lambda * (1 - ||grad||)^2 still for each single sample
        gradient_penalty = K.square(1 - gradient_l2_norm)
        # return the mean as loss over all the batch samples
        return K.mean(gradient_penalty)


    def wasserstein_loss(self, y_true, y_pred):
        return K.mean(y_true * y_pred)

    def build_generator(self):

        # noise = Input(shape=(self.latent_dim,))
        text_input = Input(shape=(self.text_dim,))
        # text_emb = Dense(256, activation='relu')(text_input)
        text_with_noise = GaussianNoise(stddev=0.5)(text_input)
        x = Dense(4*4*256)(text_with_noise)
        x = Reshape((4, 4, 256))(x)
        x = BatchNormalization(momentum = 0.9)(x)
        x = LeakyReLU()(x)
        x = Conv2DTranspose(256, kernel_size=5)(x)
        x = BatchNormalization(momentum = 0.9)(x)
        x = LeakyReLU()(x)
        x = UpSampling2D(size=(2, 2))(x)
        x = BatchNormalization(momentum = 0.9)(x)
        x = LeakyReLU()(x)
        x = Conv2DTranspose(128, kernel_size=5, padding='same')(x)
        x = UpSampling2D(size=(2, 2))(x)
        x = BatchNormalization(momentum = 0.9)(x)
        x = LeakyReLU()(x)
        x = Conv2DTranspose(64, kernel_size=5, padding='same')(x)
        x = UpSampling2D(size=(2, 2))(x)
        x = BatchNormalization(momentum = 0.9)(x)
        x = Conv2D(3 ,kernel_size=5, padding='same')(x)
        gen_output = Activation('tanh')(x)
        gen = Model(inputs=text_input, outputs=gen_output)
        gen.summary()
        return gen

    def build_critic(self):

        image_input = Input(shape=(self.img_shape))
        text_with_noise = Input(shape=(self.text_dim,))
        text_input = Activation('softmax')(text_with_noise)
        text_emb = Dense(256, activation='relu')(text_input)

        x = Conv2D(64 ,kernel_size=3, padding='same', kernel_initializer='he_normal')(image_input)
        x = LeakyReLU()(x)
        x = Conv2D(64 ,kernel_size=4, padding='same', kernel_initializer='he_normal', strides=(2, 2))(x)
        x = LeakyReLU()(x)

        x = Conv2D(128 ,kernel_size=5, kernel_initializer='he_normal')(x)
        x = LeakyReLU()(x)
        x = Conv2D(128 ,kernel_size=5, kernel_initializer='he_normal', strides=(2, 2))(x)
        x = LeakyReLU()(x)

        x = Conv2D(256 ,kernel_size=5, kernel_initializer='he_normal')(x)
        x = LeakyReLU()(x)
        x = Conv2D(256 ,kernel_size=5, kernel_initializer='he_normal', strides=(2, 2))(x)
        x = LeakyReLU()(x)

        image_feat = Flatten()(x)
        y = concatenate([image_feat, text_emb])
        y = LeakyReLU()(y)
        dis_output = Dense(1, kernel_initializer='he_normal')(y)
        dis = Model(inputs=[image_input, text_with_noise], outputs=dis_output)
        dis.summary()
        return dis

    def train(self, epochs, sample_interval=200):

        # Load the dataset ans rescale to -1 to 1
        data, tags = read_preprocessimg()
        tag_set = set(tags)
        
        t = time()
        for epoch in range(epochs):

            for _ in range(self.n_critic):

                # ---------------------
                #  Train Discriminator
                # ---------------------

                # Select a random batch of images
                idx = np.random.choice(len(data))
                batch_imgs = data[idx].x
                batch_tag = data[idx].y
                batch_size = len(batch_tag)

                # Sample generator input
                other_tag = tag_set - set([idx])
                fake_tags = np.random.choice(list(other_tag) * 3, batch_size)
                fake_tags = to_categorical(fake_tags, num_classes=130)

                # batch ground truth
                valid = np.ones(batch_size)
                fake = -np.ones(batch_size)
                false_tag = -np.ones(batch_size)
                dummy = np.random.uniform(-0.1, 0.1, batch_size)
                
                # Train the critic
                d_loss = self.critic_model.train_on_batch([batch_imgs, batch_tag, fake_tags],
                                                                [valid, fake, false_tag, dummy])

            # ---------------------
            #  Train Generator
            # ---------------------
            
            
            gen_valid = np.ones(batch_size)
            idx = np.random.choice(len(tags) - batch_size)
            target_tag = tags[idx: idx + batch_size]
            target_tag = to_categorical(target_tag, num_classes=130)

            g_loss = self.generator_model.train_on_batch(target_tag, gen_valid)

            # Plot the progress
            

            # If at save interval => save generated image samples
            if epoch % sample_interval == 0:
                for i in range(len(data)):
                    np.random.shuffle(data[i].x)
                print ("%d [D loss: %f] [G loss: %f] [Time usage: %f]" % (epoch, d_loss[0], g_loss, time() - t))
                t = time()
                self.sample_images(list(tag_set), epoch)
            
            if epoch % (sample_interval * 20) == 0:
                self.save(epoch)

    def sample_images(self, tags, epoch):
        target_tags = np.array([np.random.choice(tags, 10)] * 10).flatten()
        # print(target_tags)
        noisy_target_tag = np.random.normal(0, 1, size=(100, self.text_dim))
        for i, idx in enumerate(target_tags):
            noisy_target_tag[i][idx] = 10
        gen_img = self.generator.predict(noisy_target_tag)
        gen_img = (gen_img + 1) / 2
        np.clip(gen_img, 0, 1)

        fig, ax = plt.subplots(10, 10)

        for i in range(10):
            for j in range(10):
                ax[i, j].imshow(gen_img[i * 10 + j])
                ax[i, j].axis('off')
        plt.savefig('%sgraphic/%d.jpg' % (home, epoch))
        plt.close('all')

    def save(self, epoch):
        self.critic.save('%smodel/critic%d.hdf5' % (home, epoch))
        self.generator.save('%smodel/generator%d.hdf5' % (home, epoch))

if __name__ == '__main__':
    wgan = WGANGP()
    wgan.train(epochs=30000, sample_interval=100)
