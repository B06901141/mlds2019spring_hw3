import os
import pickle
from collections import Counter
from multiprocessing import Pool
from time import time

import keras.backend as K
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from keras import layers
from keras.layers import (Activation, AveragePooling2D, BatchNormalization,
                          Conv2D, Conv2DTranspose, Dense, Dropout, Flatten,
                          GaussianNoise, GlobalMaxPool2D, Input, Lambda,
                          LeakyReLU, Reshape, UpSampling2D, concatenate)
from keras.models import Model, Sequential, load_model
from keras.optimizers import Adam, Optimizer, RMSprop
from keras.utils import to_categorical, plot_model
from skimage.io import imread

# %matplotlib inline
from const import *
home = ''
from tqdm import tqdm

def get_faces_tags(filename):
    data = pd.read_csv(filename)
    tags = data[data.columns[1]]
    delimeter = '	'
    attr = []
    for i in range(len(tags)):
        attr.extend(tags[i].split(delimeter)[:-1])
    print(tags[1].split(delimeter))
    print(len(list(set(attr))))
    print(len(attr))

def get_images_tags(filename):
    def mapdata(tag):
        return tag_to_idx[tag]
    data = pd.read_csv(filename)
    tags = data[data.columns[1]]
    tags = [d for d in map(mapdata, data[data.columns[1]])]
    return tags

def fn2arr(filename):
    img = imread(filename)
    return img

def read_images(folder):
    file_list = os.listdir(folder)
    def fn(filename):
        fp = os.path.join(folder, filename)
        return fp
    p = Pool(processes=12)
    images = p.map(fn2arr, map(fn, file_list))
    p.close()
    p.join()
    return images

def sampling(args):
    emb = args
    batch = K.shape(emb)[0]
    dim = K.int_shape(emb)[1]
    # by default, random_normal has mean = 0 and std = 1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return emb * epsilon

def wgan_loss(y_true, y_pred):
    return K.mean(y_pred * y_true)

def get_gan(input_shape=(64,64,3), text_dim = (130,), noise_dim = (128,)):
    op = Adam(lr=1e-5, beta_1 = 0.5)
    
    ##### generator
    text_input = Input(shape=text_dim)
    text_emb = Dense(256, activation='relu')(text_input)
    noisy_text_input = GaussianNoise(stddev = 0.5)(text_emb)
    x = Dense(4*4*256)(noisy_text_input)
    x = Reshape((4, 4, 256))(x)
    
    x = LeakyReLU()(x)
    x = Conv2DTranspose(256, kernel_size=5)(x)
    
    x = LeakyReLU()(x)
    x = UpSampling2D(size=(2, 2))(x)
    
    x = LeakyReLU()(x)
    x = Conv2DTranspose(128, kernel_size=5, padding='same')(x)
    x = Conv2DTranspose(128, kernel_size=5, padding='same')(x)
    x = UpSampling2D(size=(2, 2))(x)
    
    x = LeakyReLU()(x)
    x = Conv2DTranspose(64, kernel_size=5, padding='same')(x)
    x = Conv2DTranspose(64, kernel_size=5, padding='same')(x)
    x = UpSampling2D(size=(2, 2))(x)
    
    x = Conv2DTranspose(3, kernel_size=5, padding='same')(x)
    gen_output = Activation('tanh')(x)
    gen = Model(inputs=text_input, outputs=gen_output)
    gen.summary()
    gen.compile(op, loss='binary_crossentropy', metrics=['accuracy'])
    #####

    ##### discriminator
    image_input = Input(shape=input_shape)
    dis_text_input = Input(shape=text_dim)
    text_emb = Dense(256, activation='relu')(dis_text_input)

    x = Conv2D(64 ,kernel_size=3, padding='same', kernel_initializer='he_normal')(image_input)
    x = LeakyReLU()(x)
    x = Conv2D(64 ,kernel_size=4, padding='same', kernel_initializer='he_normal')(x)
    x = LeakyReLU()(x)
    x = AveragePooling2D()(x)

    x = Conv2D(128 ,kernel_size=5, kernel_initializer='he_normal')(x)
    x = LeakyReLU()(x)
    x = Conv2D(128 ,kernel_size=5, kernel_initializer='he_normal')(x)
    x = LeakyReLU()(x)
    x = AveragePooling2D()(x)

    x = Conv2D(256 ,kernel_size=5, kernel_initializer='he_normal')(x)
    x = LeakyReLU()(x)
    x = Conv2D(256 ,kernel_size=5, kernel_initializer='he_normal')(x)
    x = LeakyReLU()(x)
    x = AveragePooling2D()(x)

    image_feat = Flatten()(x)
    y = concatenate([image_feat, text_emb])
    y = LeakyReLU()(y)
    dis_output = Dense(1, kernel_initializer='he_normal')(y)
    dis = Model(inputs=[image_input, dis_text_input], outputs=dis_output)
    dis.summary()
    dis.compile(op, loss='binary_crossentropy', metrics=['accuracy'])
    #####

    fake = gen(text_input)
    dis.trainable = False
    outputs = dis([fake, noisy_text_input])
    model = Model(inputs=text_input, outputs=outputs)
    model.compile(optimizer=op, loss='binary_crossentropy', metrics=['accuracy'])
    # model.compile(optimizer=Adam(lr=1e-5, beta_1=0.5), loss='binary_crossentropy', metrics=['accuracy'])
    model.summary()
    return model, gen, dis

def acgan(input_shape=(64,64,3), text_dim = (130,), noise_dim = (128,)):
    op = Adam(lr=1e-5, beta_1 = 0.5)
    
    ##### generator
    text_input = Input(shape=text_dim)
    noise = Input(shape=noise_dim)
    text_emb = Dense(256, kernel_initializer='orthogonal')(text_input)
    text_aug = Dense(256)(text_input)
    noisy_text_input = Lambda(sampling, output_shape=(256,), name='z')(text_aug)
    x = layers.add([text_emb, noisy_text_input])
    x = layers.concatenate([x, noise])
    x = Dense(4*4*256)(x)
    x = Reshape((4, 4, 256))(x)
    
    x = LeakyReLU()(x)
    x = Conv2DTranspose(256, kernel_size=5)(x)
    
    x = LeakyReLU()(x)
    x = UpSampling2D(size=(2, 2))(x)
    
    x = LeakyReLU()(x)
    x = Conv2DTranspose(128, kernel_size=5, padding='same')(x)
    x = Conv2DTranspose(128, kernel_size=5, padding='same')(x)
    x = UpSampling2D(size=(2, 2))(x)
    
    x = LeakyReLU()(x)
    x = Conv2DTranspose(64, kernel_size=5, padding='same')(x)
    x = Conv2DTranspose(64, kernel_size=5, padding='same')(x)
    x = UpSampling2D(size=(2, 2))(x)
    
    x = Conv2DTranspose(3, kernel_size=5, padding='same')(x)
    gen_output = Activation('tanh')(x)
    gen = Model(inputs=[noise, text_input], outputs=gen_output)
    gen.summary()
    gen.compile(op, loss='binary_crossentropy', metrics=['accuracy'])
    #####

    ##### discriminator
    image_input = Input(shape=input_shape)
    dis_text_input = Input(shape=text_dim)
    text_emb = Dense(256, activation='relu')(dis_text_input)

    x = Conv2D(64 ,kernel_size=3, padding='same')(image_input)
    x = LeakyReLU()(x)
    x = Conv2D(64 ,kernel_size=4, padding='same')(x)
    x = LeakyReLU()(x)
    x = AveragePooling2D()(x)

    x = Conv2D(128 ,kernel_size=5)(x)
    x = LeakyReLU()(x)
    x = Conv2D(128 ,kernel_size=5)(x)
    x = LeakyReLU()(x)
    x = AveragePooling2D()(x)

    x = Conv2D(256 ,kernel_size=5)(x)
    x = LeakyReLU()(x)
    x = Conv2D(256 ,kernel_size=5)(x)
    clf_input = LeakyReLU()(x)

    text_emb = Reshape((4, 4, 16))(text_emb)
    y = concatenate([clf_input, text_emb])
    y = Conv2D(512 ,kernel_size=1)(y)
    y = Flatten()(y)
    y = LeakyReLU()(y)
    dis_output = Dense(1)(y)

    ##### classifier
    '''
    classifier use Maxpooling for small gradient
    '''
    z = GlobalMaxPool2D()(clf_input)
    z = Dense(130)(z)
    z = LeakyReLU()(z)
    z = Dense(130)(z)
    clf_output = Activation('softmax')(z)
    ##### end of classifier

    dis = Model(inputs=[image_input, dis_text_input], outputs=[dis_output, clf_output])
    dis.summary()
    dis.compile(op, loss=['binary_crossentropy', 'binary_crossentropy'], loss_weights=[10, 1])
    ##### end of discriminator

    fake = gen([noise, text_input])
    dis.trainable = False
    outputs = dis([fake, text_input])
    model = Model(inputs=[noise, text_input], outputs=outputs)
    model.compile(optimizer=op, loss=['binary_crossentropy', 'binary_crossentropy'], loss_weights=[10, 1])
    # model.compile(optimizer=Adam(lr=1e-5, beta_1=0.5), loss='binary_crossentropy', metrics=['accuracy'])
    model.summary()
    return model, gen, dis

def generate_images(gen, tags, epoch):
    noise = np.random.normal(0, 1, size=(100, 128))
    target_tags = np.array([np.random.choice(tags, 10)] * 10).flatten()
    # print(target_tags)
    target_tags = to_categorical(target_tags, num_classes=130)
    
    gen_img = gen.predict([noise, target_tags])
    gen_img = (gen_img + 1) / 2
    np.clip(gen_img, 0, 1)
    fig, ax = plt.subplots(10, 10)
    
    for axi in ax.ravel():
        axi.set_axis_off()
    for i in range(10):
        for j in range(10):
            ax[i, j].imshow(gen_img[i * 10 + j])
    plt.savefig('%sgraphic/%d.jpg' % (home, epoch))
    plt.close('all')

def predict(gen, tofile = '%sgraphic/predict.jpg' % (home)):
    tags = ['pink hair black eyes', 'black hair purple eyes', 'red hair red eyes', 
             'aqua hair green eyes', 'blonde hair orange eyes']
    tags = [tag_to_idx[tag] for tag in tags] * 5
    tags = to_categorical(tags, num_classes=130)
    noise = np.random.normal(0, 1, size=(25, 128))
    

    gen_img = gen.predict([noise, tags])
    gen_img = (gen_img + 1) / 2
    np.clip(gen_img, 0, 1)
    print(np.max(gen_img), np.min(gen_img))
    fig, ax = plt.subplots(5, 5)
    
    for i in range(5):
        for j in range(5):
            ax[i, j].imshow(gen_img[i * 5 + j])
            ax[i, j].axis('off')
    plt.savefig(tofile)
    plt.close('all')

def random25_result(gen):
    data = pd.read_csv('%stags.csv' % home)
    tags = list(set(data[data.columns[1]]))
    
    for tag in tags:
        idx = tag_to_idx[tag]
        idx = to_categorical([idx] * 25, num_classes=130)
        noise = np.random.normal(0, 1, size=(25, 128))


        gen_img = gen.predict([noise, idx])
        gen_img = (gen_img + 1) / 2
        np.clip(gen_img, 0, 1)
        fig, ax = plt.subplots(5, 5)
    
        for i in range(5):
            for j in range(5):
                ax[i, j].imshow(gen_img[i * 5 + j])
                ax[i, j].axis('off')
        plt.savefig('%sgraphic/%s.jpg' % (home, tag))
        plt.close('all')

class Pair:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def read_preprocessimg():
    '''
    make images in (-1, 1)
    '''
    tags = get_images_tags('%stags.csv' % home)
    # images = pickle.load(open('%simages.pkl' % home, 'rb'))
    images = read_images('extra_data/images')
    images = (np.array(images).astype('float32') - 127.5) / 127.5
    paired = [(images[i], tags[i])  for i in range(len(images))]
    np.random.shuffle(paired)
    
    x = [p[0]  for p in paired]
    y = [p[1]  for p in paired]
    # y = to_categorical(y, num_classes=130)

    '''
    tag_count = Counter(tags)
    start = 0
    for i, num in tag_count.items():
        same_tag_image = images[start: start + num]
        # processed_tag = np.random.normal(0, 1, size=(num, 130))
        # for i in range(num):
        #     processed_tag[i][tags[start + i]] = 10
        data.append(Pair(same_tag_image, to_categorical(tags[start: start + num], num_classes=130)))
        start += num
    
    '''
    np.random.shuffle(tags)
    return (x, y), tags

def pretrained_gen(gen, input_shape=(64,64,3), text_dim = (130,), noise_dim = (128,)):
    noise = Input(noise_dim)
    text_input = Input(text_dim)
    op = RMSprop(lr=1e-5, rho=0.5)

    ##### discriminator
    image_input = Input(shape=input_shape)
    dis_text_input = Input(shape=text_dim)
    text_emb = Dense(256, activation='relu')(dis_text_input)

    x = Conv2D(64 ,kernel_size=3, padding='same')(image_input)
    x = LeakyReLU()(x)
    x = Conv2D(64 ,kernel_size=4, padding='same')(x)
    x = LeakyReLU()(x)
    x = AveragePooling2D()(x)

    x = Conv2D(128 ,kernel_size=5)(x)
    x = LeakyReLU()(x)
    x = Conv2D(128 ,kernel_size=5)(x)
    x = LeakyReLU()(x)
    x = AveragePooling2D()(x)

    x = Conv2D(256 ,kernel_size=5)(x)
    x = LeakyReLU()(x)
    x = Conv2D(256 ,kernel_size=5)(x)
    clf_input = LeakyReLU()(x)

    text_emb = Reshape((4, 4, 16))(text_emb)
    y = concatenate([clf_input, text_emb])
    y = Conv2D(512 ,kernel_size=1)(y)
    y = Flatten()(y)
    y = LeakyReLU()(y)
    dis_output = Dense(1)(y)

    ##### classifier
    '''
    classifier use Maxpooling for small gradient
    '''
    z = GlobalMaxPool2D()(clf_input)
    z = Dense(130)(z)
    z = LeakyReLU()(z)
    z = Dense(130)(z)
    clf_output = Activation('softmax')(z)
    ##### end of classifier

    dis = Model(inputs=[image_input, dis_text_input], outputs=[dis_output, clf_output])
    dis.summary()
    dis.compile(op, loss=['binary_crossentropy', 'binary_crossentropy'], loss_weights=[1, 1])
    ##### end of discriminator

    fake = gen([noise, text_input])
    dis.trainable = False
    outputs = dis([fake, text_input])
    model = Model(inputs=[noise, text_input], outputs=outputs)
    model.compile(optimizer=op, loss=['binary_crossentropy', 'binary_crossentropy'], loss_weights=[1, 1])
    # model.compile(optimizer=Adam(lr=1e-5, beta_1=0.5), loss='binary_crossentropy', metrics=['accuracy'])
    model.summary()
    return dis, model

def pretrain_dis(gen, dis):
    op = RMSprop(lr=1e-5, rho=0.5)
    noise = Input(noise_dim)
    text_input = Input(text_dim)
    fake = gen([noise, text_input])
    dis.trainable = False
    outputs = dis([fake, text_input])
    model = Model(inputs=[noise, text_input], outputs=outputs)
    model.compile(optimizer=op, loss=['binary_crossentropy', 'binary_crossentropy'], loss_weights=[1, 1])
    return model


if __name__ == '__main__':
    
    input_shape=(64,64,3)
    text_dim = (130,)
    noise_dim = (128,)
    
    
    # paired = [(images[i], tags[i])  for i in range(len(images))]
    data, tags = read_preprocessimg()
    np.random.shuffle(tags)

    tag_set = set(tags)

    
    gen = load_model('%smodel/generator.hdf5' % (home))
    # dis = load_model('%smodel/discriminator24000.hdf5' % (home))
    #gan = load_model('%smodel/gan%d.hdf5' % (home, 24000), custom_objects={'wgan_loss': wgan_loss})

    # idx_to_tag = dict()
    # for tag, i in tag_to_idx.items():
    #     idx_to_tag[i] = tag

    # total_output = []
    # for tag in tqdm(range(130), ncols=80):
    #     hun_sample = [tag] * 100
    #     hun_sample = to_categorical(hun_sample, num_classes=130)
    #     noise = np.random.normal(0, 1, (100, 128))
    #     fake = gen.predict([noise, hun_sample])
    #     clf_output = dis.predict([fake, hun_sample])[1]
    #     output = [np.argmax(dist) for dist in clf_output]
    #     total_output.append(output)

    # np.save('100predict.npy', total_output)

    # predict(dis, tofile='cgan_discriminator.png')

    #gan, gen, dis = acgan()
    #pretrain_gen(gen, data, tag_set)
    dis, gan = pretrained_gen(gen)
    # plot_model(dis, to_file='cgan_discriminator.png')
    # plot_model(gen, to_file='generator.png')
    # plot_model(gan, to_file='gan.png')
    
    
    epochs = 24000
    batch_size = 128
    k = 100
    length = len(tags)

    t = time()
    for i in range(epochs):
      
        for j in range(k):
            idx = np.random.choice(length - batch_size)
            batch_imgs = data[0][idx: idx + batch_size]
            batch_tag = data[1][idx: idx + batch_size]
            one_hot_tag = to_categorical(batch_tag, num_classes=130)

            noise = np.random.normal(0, 1, size=(batch_size, 128))
            fake_images = gen.predict([noise, one_hot_tag])
            
            seed = np.random.randint(low=1, high=130, size=batch_size)
            fake_tags = np.add(batch_tag, seed)
            fake_tags = [idx % 130 for idx in fake_tags]
            fake_tags = to_categorical(fake_tags, num_classes=130)
            
            l1 = dis.train_on_batch(x = [batch_imgs, one_hot_tag], 
                                    y = [np.ones(batch_size), one_hot_tag])
            l2 = dis.train_on_batch(x = [fake_images, one_hot_tag],
                                    y = [np.zeros(batch_size), one_hot_tag])
            l3 = dis.train_on_batch(x = [batch_imgs, fake_tags],
                                    y = [np.zeros(batch_size), one_hot_tag])
        
        if i > 10:
            k = 1
        
        tags_id = np.random.choice(length - batch_size)
        noise = np.random.normal(0, 1, size=(batch_size, 128))
        target_tags = tags[tags_id: tags_id + batch_size]
        target_tags = to_categorical(target_tags, num_classes=130)

        train_label = np.ones(batch_size)

        l4 = gan.train_on_batch(x=[noise, target_tags], y=[train_label, target_tags])

        if i % 100 == 0:
            np.random.shuffle(tags)

            print('On epoch %d time: %f' % (i, time() - t))
            print(l1, l2, l3, l4)
            t = time()
            generate_images(gen, list(tag_set), i)

        
    gen.save('%smodel/generator%d.hdf5' % (home, epochs))
    dis.save('%smodel/discriminator%d.hdf5' % (home, epochs))
    gan.save('%smodel/gan%d.hdf5' % (home, epochs))
    