import os
from multiprocessing import Pool
from skimage.io import imread
import pickle

import keras.backend as K
import matplotlib.pyplot as plt
import numpy as np
from keras.layers import (BatchNormalization, Conv2D, Dense, Dropout, Flatten, Input, concatenate, Activation,
                          LeakyReLU, AveragePooling2D, Reshape, Conv2DTranspose, UpSampling2D)
from keras.models import Model, Sequential, load_model
from keras.optimizers import Adam, Optimizer, RMSprop
from keras.utils import to_categorical

from const import *
import pandas as pd
from tqdm import trange
from time import time
from collections import Counter

home = '/'

def get_faces_tags(filename):
    data = pd.read_csv(filename)
    tags = data[data.columns[1]]
    delimeter = '	'
    attr = []
    for i in range(len(tags)):
        attr.extend(tags[i].split(delimeter)[:-1])
    print(tags[1].split(delimeter))
    print(len(list(set(attr))))
    print(len(attr))

def get_images_tags(filename):
    def mapdata(tag):
        return tag_to_idx[tag]
    data = pd.read_csv(filename)
    tags = data[data.columns[1]]
    tags = [d for d in map(mapdata, data[data.columns[1]])]
    return tags

def fn2arr(filename):
    img = imread(filename)
    return img

def read_images(folder):
    file_list = os.listdir(folder)
    def fn(filename):
        fp = os.path.join(folder, filename)
        return fp
    p = Pool(processes=12)
    images = p.map(fn2arr, map(fn, file_list))
    p.close()
    p.join()
    return images

def generator(noise_dim = (128,), text_dim = (130,)):
    noise = Input(shape=noise_dim)
    text_input = Input(shape=text_dim)
    text_emb = Dense(256, activation='relu')(text_input)
    x = concatenate([noise, text_emb])
    x = Dense(4*4*256)(x)
    x = Reshape((4, 4, 256))(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = Activation('relu')(x)
    x = Conv2DTranspose(256, kernel_size=5)(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = Activation('relu')(x)
    x = UpSampling2D(size=(2, 2), interpolation='bilinear')(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = Activation('relu')(x)
    x = Conv2DTranspose(128, kernel_size=5, padding='same')(x)
    x = UpSampling2D(size=(2, 2), interpolation='bilinear')(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = Activation('relu')(x)
    x = Conv2DTranspose(3, kernel_size=5, padding='same')(x)
    x = UpSampling2D(size=(2, 2), interpolation='bilinear')(x)
    x = BatchNormalization(momentum = 0.9)(x)
    output = Activation('tanh')(x)
    model = Model(inputs=[noise, text_input], outputs=output)
    model.summary()
    return model

def discriminator(input_shape=(64,64,3), text_dim = (130,)):
    image_input = Input(shape=input_shape)
    text_input = Input(shape=text_dim)
    text_emb = Dense(256, activation='relu')(text_input)
    x = Conv2D(64 ,kernel_size=5)(image_input)
    x = LeakyReLU()(x)
    x = Conv2D(128 ,kernel_size=5)(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = LeakyReLU()(x)
    x = Conv2D(256 ,kernel_size=5)(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = LeakyReLU()(x)
    x = Conv2D(512 ,kernel_size=5)(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = LeakyReLU()(x)
    image_feat = Flatten()(x)
    y = concatenate([image_feat, text_emb])
    output = Dense(1, activation='sigmoid')(y)
    model = Model(inputs=[image_input, text_input], outputs=output)
    model.summary()
    return model

def wgan_loss(y_true, y_pred):
    return K.mean(y_pred * (y_true - 0.5) * 2)

def get_gan(input_shape=(64,64,3), text_dim = (130,), noise_dim = (128,)):
    w_op = Adam(lr=1e-5, beta_1=0.5, clipvalue=15, clipnorm=1)
    
    ##### generator
    noise = Input(shape=noise_dim)
    text_input = Input(shape=text_dim)
    text_emb = Dense(256, activation='relu')(text_input)
    x = concatenate([noise, text_emb])
    x = Dense(4*4*256)(x)
    x = Reshape((4, 4, 256))(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = LeakyReLU()(x)
    x = Conv2DTranspose(256, kernel_size=5)(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = LeakyReLU()(x)
    x = UpSampling2D(size=(2, 2), interpolation='bilinear')(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = LeakyReLU()(x)
    x = Conv2DTranspose(128, kernel_size=5, padding='same')(x)
    x = UpSampling2D(size=(2, 2), interpolation='bilinear')(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = LeakyReLU()(x)
    x = Conv2DTranspose(64, kernel_size=5, padding='same')(x)
    x = UpSampling2D(size=(2, 2), interpolation='bilinear')(x)
    x = BatchNormalization(momentum = 0.9)(x)
    x = Conv2D(3 ,kernel_size=5, padding='same')(x)
    gen_output = Activation('tanh')(x)
    gen = Model(inputs=[noise, text_input], outputs=gen_output)
    gen.summary()
    gen.compile(Adam(lr=1e-5, beta_1=0.7), loss=wgan_loss, metrics=['accuracy'])
    #####

    ##### discriminator
    image_input = Input(shape=input_shape)
    text_input = Input(shape=text_dim)
    text_emb = Dense(256, activation='relu')(text_input)

    x = Conv2D(64 ,kernel_size=3, padding='same', kernel_initializer='he_normal')(image_input)
    x = LeakyReLU()(x)
    x = Conv2D(64 ,kernel_size=4, padding='same', kernel_initializer='he_normal')(image_input)
    x = LeakyReLU()(x)
    x = AveragePooling2D()(x)

    x = Conv2D(128 ,kernel_size=5, kernel_initializer='he_normal')(x)
    x = LeakyReLU()(x)
    x = Conv2D(128 ,kernel_size=5, kernel_initializer='he_normal')(x)
    x = LeakyReLU()(x)
    x = AveragePooling2D()(x)

    x = Conv2D(256 ,kernel_size=5, kernel_initializer='he_normal')(x)
    x = LeakyReLU()(x)
    x = Conv2D(256 ,kernel_size=5, kernel_initializer='he_normal')(x)
    x = LeakyReLU()(x)
    x = AveragePooling2D()(x)

    image_feat = Flatten()(x)
    y = concatenate([image_feat, text_emb])
    y = LeakyReLU()(y)
    dis_output = Dense(1, kernel_initializer='he_normal')(y)
    dis = Model(inputs=[image_input, text_input], outputs=dis_output)
    dis.summary()
    dis.compile(Adam(lr=1e-5, beta_1=0.5), loss=wgan_loss, metrics=['accuracy'])
    #####

    fake = gen([noise, text_input])
    dis.trainable = False
    outputs = dis([fake, text_input])
    model = Model(inputs=[noise, text_input], outputs=outputs)
    model.compile(optimizer=Adam(lr=1e-5, beta_1=0.5), loss=wgan_loss, metrics=['accuracy'])
    # model.compile(optimizer=Adam(lr=1e-5, beta_1=0.5), loss='binary_crossentropy', metrics=['accuracy'])
    model.summary()
    return model, gen, dis


def generate_images(gen, tags, epoch):
    noise = np.random.normal(0, 1, size=(100, 128))
    target_tags = np.array([np.random.choice(tags, 10)] * 10).flatten()
    # print(target_tags)
    target_tags = to_categorical(target_tags, num_classes=130)
    gen_img = gen.predict([noise, target_tags])
    gen_img = (gen_img + 1) / 2
    
    fig, ax = plt.subplots(10, 10)
    
    for axi in ax.ravel():
        axi.set_axis_off()
    for i in range(10):
        for j in range(10):
            ax[i, j].imshow(gen_img[i * 10 + j])
    plt.savefig('%sgraphic/%d.jpg' % (home, epoch))
    plt.close('all')

class Pair:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def read_preprocessimg():
    '''
    make images in (-1, 1)
    '''
    tags = get_images_tags('%stags.csv' % home)
    images = pickle.load(open('%simages.pkl' % home, 'rb'))
    images = (images - 127.5) / 127.5
    # paired = [(images[i], tags[i])  for i in range(len(images))]
    # np.random.shuffle(paired)
    
    data = []
    tag_count = Counter(tags)
    start = 0
    for i, num in tag_count.items():
        same_tag_image = images[start: start + num]
        processed_tag = to_categorical(tags[start: start + num], num_classes=130)
        data.append(Pair(same_tag_image, processed_tag))
        start += num
    return data, tags

if __name__ == '__main__':
    
    input_shape=(64,64,3)
    text_dim = (130,)
    noise_dim = (128,)
    
    
    # paired = [(images[i], tags[i])  for i in range(len(images))]
    data, tags = read_preprocessimg()
    np.random.shuffle(tags)
    processed_tags = to_categorical(tags, num_classes=130)
    tag_set = set(tags)

    #gen = load_model('%smodel/generator.hdf5' % home, custom_objects={'wgan_loss': wgan_loss})
    #dis = load_model('%smodel/discriminator.hdf5' % home, custom_objects={'wgan_loss': wgan_loss})
    #gan = load_model('%smodel/origan_gan.hdf5' % home, custom_objects={'wgan_loss': wgan_loss})
    
    gan, gen, dis = get_gan()
    epochs = 2000
    batch_size = 256
    k = 1
    length = len(tags)
    train_label = np.ones(batch_size)

    t = time()
    for i in range(epochs):
      
        for j in range(k):
            idx = np.random.choice(len(data))
            batch_imgs = data[idx].x
            batch_tag = data[idx].y
            noise = np.random.normal(0, 1, size=(len(batch_tag), 128))
            fake_images = gen.predict([noise, batch_tag])
            other_tag = tag_set - set([idx])
            dis.train_on_batch(x = [batch_imgs, batch_tag], y = np.ones(len(batch_tag)))
            dis.train_on_batch(x = [fake_images, batch_tag], y = np.zeros(len(batch_tag)))
          
            fake_tags = np.random.choice(list(tag_set) * 3, len(batch_tag))
            fake_tags = to_categorical(fake_tags, num_classes=130)
            dis.train_on_batch(x = [batch_imgs, fake_tags], y = np.zeros(len(batch_tag)))
        
        noise = np.random.normal(0, 1, size=(batch_size, 128))
        tags_id = np.random.choice(length - batch_size)
        target_tags = processed_tags[tags_id: tags_id + batch_size]
        gan.train_on_batch(x=[noise, target_tags], y=train_label)
        if i % 50 == 0:
            print('On epoch %d time: %f' % (i, time() - t))
            t = time()
            generate_images(gen, tags, i)

    gen.save('%smodel/generator.hdf5' % home)
    dis.save('%smodel/discriminator.hdf5' % home)
    gan.save('%smodel/origan_gan.hdf5' % home)
