# MLDS HW3_2

## Environment
python=3.6
keras=2.2.4
tensorflow=1.13.1
matplotlib=3.0.3
numpy=1.16.3
pandas=0.24.2
tqdm=4.28.1
scikit-image=0.15.0

## Dataset
[extra_data](https://drive.google.com/file/d/1tpW7ZVNosXsIAWu8-f5EpwtF3ls3pb79/view)

## Usage
Download extra_data from link [extra_data](https://drive.google.com/file/d/1tpW7ZVNosXsIAWu8-f5EpwtF3ls3pb79/view) to hw3_2/
Use `python acgan.py` to train model
model will be save in model/*.hdf5
