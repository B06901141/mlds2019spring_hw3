# -*- coding = utf-8 -*-
import os, sys

import keras.backend as K
import matplotlib.pyplot as plt
import numpy as np
from keras.layers import (BatchNormalization, Conv2D, Dense, Dropout, Flatten, UpSampling2D, Conv2DTranspose,
                          InputLayer, LeakyReLU, MaxPooling2D, Reshape)
from keras.models import Sequential
from keras.optimizers import Adam, RMSprop, Optimizer, SGD

from dataProcess import load_data

data_folder = './faces'

"""
cfg = K.tf.ConfigProto()
cfg.gpu_options.allow_growth = True
K.set_session(K.tf.Session(config=cfg))
"""
test = '04'
if not os.path.exists('./model'+test):os.makedirs('./model'+test)

class GAN:
    def __init__(self, lr=1e-4):
        self.shape = (96, 96, 3)
        #original:lr=0.001, beta_1=0.9, beta_2=0.999
        self.optimizer = Adam(lr=lr, beta_1=0.9, beta_2=0.999)

        self.G = self.generator()
        self.D = self.discriminator()

        self.G.compile(loss='binary_crossentropy', optimizer=self.optimizer)#doesn't matter
        self.D.compile(loss='binary_crossentropy', optimizer=SGD(lr=lr*10, momentum=0.0, decay=0.0, nesterov=False))

        self.stacked_GD = self.stacked_GD()

        self.stacked_GD.compile(loss='binary_crossentropy', optimizer=self.optimizer)

    def generator(self):
        """ Declare generator """
        model = Sequential()
        model.add(InputLayer((100,)))
        model.add(Dense(256*12*12))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization())
        #model.add(Dropout(0.2))
        model.add(Reshape((12,12,256)))

        model.add(UpSampling2D())
        model.add(Conv2DTranspose(256, 
                         kernel_size=4,
                         padding='same'))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization())
        #model.add(Dropout(0.2))

        model.add(UpSampling2D())
        model.add(Conv2DTranspose(128, 
                         kernel_size=4,
                         padding='same'))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization())
        #model.add(Dropout(0.2))

        model.add(UpSampling2D())
        model.add(Conv2DTranspose(64, 
                         kernel_size=4,
                         padding='same'))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization())
        #model.add(Dropout(0.2))

        model.add(Conv2DTranspose(3, 
                         kernel_size=4,
                         padding='same',
                         activation='tanh'))

        return model

    def discriminator(self):
        """ Declare discriminator """
        model = Sequential()
        model.add(InputLayer(self.shape))

        model.add(Conv2D(filters=32,
                         kernel_size=4))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.2))

        model.add(Conv2D(filters=64,
                         kernel_size=4))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.2))

        model.add(Conv2D(filters=128,
                         kernel_size=4))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.2))

        model.add(Conv2D(filters=256,
                         kernel_size=4))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.2))

        model.add(Flatten())

        model.add(Dense(1, activation='sigmoid'))

        return model

    def stacked_GD(self):
        self.D.trainable = False

        model = Sequential()
        model.add(self.G)
        model.add(self.D)

        return model

    def save_weights(self, G_weights_name='model/G_weights.h5', D_weights_name='model/D_weights.h5'):
        try:os.mkdir('model')
        except:pass
        self.G.save_weights(G_weights_name)
        self.D.save_weights(D_weights_name)

    def load_weights(self, G_weights_name='model/G_weights.h5', D_weights_name='model/D_weights.h5'):
        self.G.load_weights(G_weights_name)
        self.D.load_weights(D_weights_name)

    def plot_images(self, save2file=False, filename=None, samples=25, epoch=0, batch=0, seed=None):
        ''' Plot and generated images '''
        if seed is not None:np.random.seed(seed)
        if not os.path.exists("./images"):
            os.makedirs("./images")
        if filename == None:
            filename = "./images/epoch_%d_batch_%d.png" % (epoch, batch)
        else:pass
        noise = np.random.normal(0, 1, (samples, 100))

        images = (self.G.predict(noise)+1)/2.0

        plt.figure(figsize=(20, 20))

        for i in range(images.shape[0]):
            plt.subplot(round(samples**0.5), round(samples**0.5), i+1)
            image = images[i, :, :, :]
            plt.imshow(image)
            plt.axis('off')
        plt.tight_layout()

        if save2file:
            plt.savefig(filename)
            plt.close('all')
        else:
            plt.show()

    def train(self, X_train=None, epochs=10, batch=32, sample_interval=10, g_batchs=5, d_batchs=5, load=False):
        data_size = len(os.listdir(data_folder)
                        ) if X_train is None else len(X_train)        

        if load:self.load_weights(G_weights_name='model'+test+'/G_weights.h5', D_weights_name='model'+test+'/D_weights.h5')
        for epoch in range(1, epochs+1):
            # train discriminator
            g_loss = np.inf
            d_loss = np.inf
            for d_bat in range(d_batchs):
                if d_loss < 0.05:break
                #discriminator training data
                sub_index_list = np.random.randint(0, data_size, batch)
                true_images = load_data(
                    sub_index_list) if X_train is None else X_train[sub_index_list]

                gen_noise = np.random.normal(loc=0, scale=0.5, size=(batch, 100))
                false_images = self.G.predict(gen_noise)

                #normalize to [-1,1]
                true_images = true_images*2-1

                print('Epoch: %2d/%2d  ' % (epoch, epochs), end='', flush=True)
                #print('batch: %3d/%3d  ' %
                #      (dbat+1, d_batchs+g_batchs), end='', flush=True)

                d_loss_true = self.D.train_on_batch(true_images, np.ones((batch, 1))-np.random.rand(batch,1)*0.05)
                d_loss_false = self.D.train_on_batch(false_images, np.zeros((batch, 1))+np.random.rand(batch,1)*0.05)
                d_loss = d_loss_true+d_loss_false

                print('d_loss:%3.3f' %(d_loss), end='\r', flush=True)
                
            #train generator
            for g_bat in range(g_batchs):
                if g_loss < 0.1:break
                #generator training data
                noise = np.random.normal(0, 1, (batch, 100))
                y_mislabled = np.ones((batch, 1))-np.random.rand(batch,1)*0.2

                print('Epoch: %2d/%2d  ' % (epoch, epochs), end='', flush=True)
                #print('batch: %3d/%3d  ' %
                #      (d_batchs+gbat+1, d_batchs+g_batchs), end='', flush=True)

                g_loss = self.stacked_GD.train_on_batch(noise, y_mislabled)

                print('g_loss: %3.3f' %(g_loss), end='\r', flush=True)
                

            print('Epoch: %2d/%2d  d_loss:%3.3f  g_loss: %3.3f' % (epoch, epochs, d_loss, g_loss))


            if epoch % sample_interval == 0:
                self.plot_images(save2file=True, epoch=epoch, seed=45624)
                self.save_weights(G_weights_name='model'+test+'/tmp_G_weights.h5', D_weights_name='model'+test+'/tmp_D_weights.h5')
        
        self.save_weights(G_weights_name='model'+test+'/G_weights.h5', D_weights_name='model'+test+'/D_weights.h5')

def save_imgs(generator, filename = 'output.png', seed = False):
    import matplotlib.pyplot as plt
    r, c = 5, 5
    if seed:np.random.seed(seed)
    noise = np.random.normal(0, 1, (r * c, 100))
    # gen_imgs should be shape (25, 64, 64, 3)
    gen_imgs = (generator.predict(noise)+1)/2.0
    fig, axs = plt.subplots(r, c)
    cnt = 0
    for i in range(r):
        for j in range(c):
            axs[i,j].imshow(gen_imgs[cnt, :,:,:])
            axs[i,j].axis('off')
            cnt += 1
    fig.savefig(filename)
    plt.close()

if __name__ == '__main__':    
    gan = GAN(lr=1e-4)
    if sys.argv[1] == 'trained':
        gan.load_weights(G_weights_name='model'+test+'/trained_G_weights.h5', D_weights_name='model'+test+'/trained_D_weights.h5')    
    """
    #plot model structure
    from keras.utils import plot_model
    plot_model(gan.G, to_file='model'+test+'/G.png')
    plot_model(gan.D, to_file='model'+test+'/D.png')
    """
    if sys.argv[1] == 'train':
        gan.load_weights(G_weights_name='model'+test+'/tmp_G_weights.h5', D_weights_name='model'+test+'/tmp_D_weights.h5')
        gan.train(epochs=160, batch=32, sample_interval=1, g_batchs=4, d_batchs=6)#check, don't tune g and d batchs
        #gan.train(epochs=160, batch=32, sample_interval=2, g_batchs=5, d_batchs=3)#, load=True)
        #gan.train(epochs=80, batch=32, sample_interval=4, g_batchs=4, d_batchs=2)#, load=True)
        """
        gan.train(epochs=60, batch=32, sample_interval=1, g_batchs=8, d_batchs=5)#check, don't tune g and d batchs
        gan.train(epochs=120, batch=32, sample_interval=2, g_batchs=5, d_batchs=3)#, load=True)
        gan.train(epochs=60, batch=32, sample_interval=4, g_batchs=4, d_batchs=2)#, load=True)
        """
    if sys.argv[1] == 'generate':
        gan.load_weights(G_weights_name='model'+test+'/G_weights.h5', D_weights_name='model'+test+'/D_weights.h5')
    save_imgs(gan.G, filename='output'+test+'.png', seed=1824356)#seed is for reproduce 