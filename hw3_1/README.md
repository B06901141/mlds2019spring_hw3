# README  
###### tags: `MLDS` `hw3_1` `anime_generation` 

## Toolkits
+ python 3.6.8
+ numpy 1.16.2
+ keras 2.2.4
+ tensorflow-gpu 1.13.1
+ matplotlib 3.0.3

## Regenerating
> extract data in faces.tar.gz into dir faces.  
> Excute `python mainXX.py trained` to generate outputXX.png using the trained model of mine.  
> Excute `python mainXX.py train` to train, plot outputXX.png for baseline test.  
> Excute `python mainXX.py generate` to plot outputXX.png for baseline test using the above new trained model.  
> XX=01,02,03,04  

## subscript 
> for tips in https://github.com/soumith/ganhacks   
> main01.py using 1,3,4 of tips  
> main02.py using 1,3,4,6 of tips  
> main03.py using 1,3,4,9 of tips  
> main04.py using 1,3,4,14 of tips  
> lr = 1e-4, batch_size = 32  
> for converging, lr should <= 1e-4.  

## model sturcture  
#### Generator  
![./G.png](./G.png)  
#### Discriminator  
![./D.png](./D.png)  