import torch
import torch.nn as nn
import torch.nn.functional as F


class Generator(nn.Module):

    def __init__(self, latent_dim):
        super().__init__()
        self.latent_dim = latent_dim

        self.gen = nn.Sequential(
            nn.ConvTranspose2d(in_channels=self.latent_dim,
                               out_channels=1024,
                               kernel_size=4,
                               stride=1,
                               bias=False),
            nn.BatchNorm2d(1024),
            nn.LeakyReLU(negative_slope=.2),
            nn.ConvTranspose2d(in_channels=1024,
                               out_channels=512,
                               kernel_size=4,
                               stride=2,
                               padding=1,
                               bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(negative_slope=.2),
            nn.ConvTranspose2d(in_channels=512,
                               out_channels=256,
                               kernel_size=4,
                               stride=2,
                               padding=1,
                               bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(negative_slope=.2),
            nn.ConvTranspose2d(in_channels=256,
                               out_channels=128,
                               kernel_size=4,
                               stride=2,
                               padding=1,
                               bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(negative_slope=.2),
            nn.ConvTranspose2d(in_channels=128,
                               out_channels=3,
                               kernel_size=4,
                               stride=2,
                               padding=1),
            nn.Sigmoid()
        )
        return

    def forward(self, input):
        return self.gen(input.view(input.shape[0], -1, 1, 1))


class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()
        self.discrim = nn.Sequential(
            nn.Conv2d(in_channels=3,
                      out_channels=128,
                      kernel_size=4,
                      stride=2,
                      padding=1,
                      bias=False),
            nn.LeakyReLU(negative_slope=.2),
            nn.Conv2d(in_channels=128,
                      out_channels=256,
                      kernel_size=4,
                      stride=2,
                      padding=1,
                      bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(negative_slope=.2),
            nn.Conv2d(in_channels=256,
                      out_channels=512,
                      kernel_size=4,
                      stride=2,
                      padding=1,
                      bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(negative_slope=.2),
            nn.Conv2d(in_channels=512,
                      out_channels=1024,
                      kernel_size=4,
                      stride=2,
                      padding=1,
                      bias=False),
            nn.BatchNorm2d(1024),
            nn.LeakyReLU(negative_slope=.2),
            nn.Conv2d(in_channels=1024,
                      out_channels=1,
                      kernel_size=4,
                      stride=1),
        )

    def forward(self, input):
        return self.discrim(input).view(-1, 1)


class MNISTGenerator(nn.Module):
    def __init__(self, g_input_dim, g_output_dim=784):
        super().__init__()
        self.latent_dim = g_input_dim
        self.fc1 = nn.Linear(g_input_dim, 256)
        self.fc2 = nn.Linear(self.fc1.out_features, self.fc1.out_features * 2)
        self.fc3 = nn.Linear(self.fc2.out_features, self.fc2.out_features * 2)
        self.fc4 = nn.Linear(self.fc3.out_features, g_output_dim)

    # forward method
    def forward(self, x):
        x = F.leaky_relu(self.fc1(x), 0.2)
        x = F.leaky_relu(self.fc2(x), 0.2)
        x = F.leaky_relu(self.fc3(x), 0.2)
        return torch.sigmoid(self.fc4(x))


class MNISTDiscriminator(nn.Module):
    def __init__(self, d_input_dim=784):
        super().__init__()
        self.fc1 = nn.Linear(d_input_dim, 1024)
        self.fc2 = nn.Linear(self.fc1.out_features, self.fc1.out_features // 2)
        self.fc3 = nn.Linear(self.fc2.out_features, self.fc2.out_features // 2)
        self.fc4 = nn.Linear(self.fc3.out_features, 1)

    # forward method
    def forward(self, x):
        x = F.leaky_relu(self.fc1(x), 0.2)
        x = F.dropout(x, 0.3)
        x = F.leaky_relu(self.fc2(x), 0.2)
        x = F.dropout(x, 0.3)
        x = F.leaky_relu(self.fc3(x), 0.2)
        x = F.dropout(x, 0.3)
        return self.fc4(x)


if __name__ == "__main__":
    gen = Generator(101)
    dis = Discriminator()
    print(dis(gen(torch.randn(13, 101))).shape)

    gen = MNISTGenerator(784, 784)
    dis = MNISTDiscriminator(784)
    print(dis(gen(torch.randn(13, 784))).shape)
