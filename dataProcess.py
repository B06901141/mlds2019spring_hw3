import os

import matplotlib.image as img
import matplotlib.pyplot as plt
import numpy as np


def show_one_img(num, channel=[0, 1, 2]):
    try:
        test = img.imread('faces/%d.jpg' % num)[:, :, 0]  # np.array
        print('shape: ', test.shape)
        plt.imshow(test, cmap='Greys_r')
        plt.show()
    except FileNotFoundError:
        print('No such file num in ./faces.')


def load_data(num=None, folder='faces'):
    # need matplotlib.image , numpy ,and os
    if num is None:
        return np.array([img.imread(folder + '/' + pic)
                         for pic in os.listdir(folder)]) / 255.0
    else:
        return np.array([img.imread(folder + '/' + str(pic) + '.jpg')
                         for pic in num]) / 255.0


if __name__ == '__main__':
    X_train = load_data([0, 1, 2, 3])

    print(X_train.shape, X_train[0, 0:10, 0:10, 0])
