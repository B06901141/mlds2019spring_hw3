import multiprocessing as mp
import os

import numpy as np
import pandas as pd
import torch
import torch.autograd as autograd
import torch.utils.data as data
from PIL import Image
from torchvision import datasets as D
from torchvision import transforms as T


class FacesDataset(data.Dataset):
    def __init__(self, folder="./faces", transform=None):
        super().__init__()
        images = [Image.open(os.path.join(folder, n))
                  for n in os.listdir(folder)]
        transform = transform if transform else T.Compose(
            [T.RandomHorizontalFlip(), T.Resize(size=64), T.ToTensor()])
        self.images = [transform(img) for img in images]

    def __len__(self):
        return len(self.images)

    def __getitem__(self, i):
        return self.images[i]


class Counter:
    def __init__(self):
        self._c = 0

    def __iadd__(self, i):
        self._c += i
        return self

    def __str__(self):
        return str(self._c)

    def __int__(self):
        return self._c


class __GradReverse(autograd.Function):
    @staticmethod
    def forward(ctx, *inputs):
        inputs = tuple(i.clone() for i in inputs)
        if len(inputs) == 1:
            inputs = inputs[0]
        return inputs

    @staticmethod
    def backward(ctx, *grads):
        return tuple(-gd for gd in grads)


class __NoGrad(autograd.Function):
    @staticmethod
    def forward(ctx, *inputs):
        inputs = tuple(i.clone() for i in inputs)
        if len(inputs) == 1:
            inputs = inputs[0]
        return inputs

    @staticmethod
    def backward(ctx, *grads):
        return tuple(None for _ in grads)


GradReverse = __GradReverse.apply
NoGrad = __NoGrad.apply
