# -*- coding = utf-8 -*-
import os

import keras.backend as K
import matplotlib.pyplot as plt
import numpy as np
from keras.layers import (BatchNormalization, Conv2D, Dense, Dropout, Flatten,
                          InputLayer, LeakyReLU, MaxPooling2D, Reshape)
from keras.models import Sequential
from keras.optimizers import RMSprop, Optimizer

from dataProcess import load_data

data_folder = './faces'

# lr=1e-4 too slow


class WGAN(object):
    def __init__(self, width=96, height=96, channels=3,
                 lr=1e-3, clipvalue=.1, clipnorm=.1):
        self.shape = (width, height, channels)
        # original:lr=0.001, rho=0.9, epsilon=None, decay=0.0
        self.Doptimizer = RMSprop(
            lr=lr, rho=0.9, epsilon=None, decay=0.0, clipvalue=clipvalue, clipnorm=clipnorm)
        self.optimizer = RMSprop(lr=lr, rho=0.9, epsilon=None, decay=0.0)

        self.G = self.__generator()
        self.D = self.__discriminator()

        self.G.compile(loss='binary_crossentropy', optimizer=self.optimizer)
        self.D.compile(loss=lambda y_true, y_pred: -K.mean(y_pred * (y_true - 0.5) * 2), optimizer=self.Doptimizer,
                       metrics=['accuracy'])

        self.stacked_GD = self.__stacked_GD()

        self.stacked_GD.compile(
            loss=lambda y_true, y_pred: -K.mean(y_pred), optimizer=self.optimizer)

    def __generator(self):
        """ Declare generator """
        model = Sequential()
        model.add(Dense(128, input_shape=(100,)))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Dropout(0.2))
        model.add(Dense(256))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Dropout(0.2))
        model.add(Dense(512))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Dropout(0.2))
        model.add(Dense(512))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Dropout(0.2))
        model.add(Dense(self.shape[0] * self.shape[1] *
                        self.shape[2], activation='sigmoid'))
        model.add(Reshape(self.shape))

        return model

    def __discriminator(self):
        """ Declare discriminator """
        model = Sequential()
        model.add(InputLayer(self.shape))
        model.add(Conv2D(filters=64,
                         kernel_size=(8, 8),
                         activation='relu'))
        model.add(Dropout(0.3))
        model.add(Conv2D(filters=64,
                         kernel_size=(8, 8),
                         activation='relu'))
        model.add(MaxPooling2D((2, 2)))
        model.add(Dropout(0.3))
        model.add(Conv2D(filters=64,
                         kernel_size=(4, 4),
                         activation='relu'))
        model.add(Dropout(0.3))
        model.add(Flatten())

        model.add(Dense(64))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dense(1, activation='relu'))
        model.summary()

        return model

    def __stacked_GD(self):
        self.D.trainable = False

        model = Sequential()
        model.add(self.G)
        model.add(self.D)

        return model

    def train(self, X_train=None, epochs=10, batch=64,
              sample_interval=50, k=5, load=False):
        data_size = len(os.listdir(data_folder)
                        ) if X_train is None else len(X_train)

        # random index
        index_list = np.array(range(data_size))
        np.random.shuffle(index_list)
        g_loss = 9999

        if load == True:
            self.load_weights()
        for epoch in range(1, epochs + 1):
            k = 0
            print('Epoch: %2d/%2d  ' % (epoch, epochs), end='', flush=True)

            for bat in range(0, data_size // batch):
                print('batch: %3d/%3d  ' %
                      (bat + 1, data_size // batch), end='', flush=True)
                k += 1
                # train discriminator
                sub_index_list = index_list[batch * bat:batch * (bat + 1)]
                true_images = load_data(
                    sub_index_list) if X_train is None else X_train[sub_index_list]

                gen_noise = np.random.normal(loc=0, scale=1, size=(batch, 100))
                false_images = self.G.predict(gen_noise)

                x_combined_batch = np.concatenate((true_images, false_images))
                y_combined_batch = np.concatenate(
                    (np.ones((batch, 1)), np.zeros((batch, 1))))

                d_loss = self.D.train_on_batch(
                    x_combined_batch, y_combined_batch)

                # train generator

                if k % 5 == 0:
                    noise = np.random.normal(0, 1, (batch * 2, 100))
                    y_mislabled = np.ones((batch * 2, 1))
                    g_loss = self.stacked_GD.train_on_batch(
                        noise, y_mislabled)

                print('d_loss:%3.3f, g_loss: %3.3f' %
                      (d_loss[0], g_loss), end='\r', flush=True)
                print('Epoch: %2d/%2d  ' % (epoch, epochs), end='', flush=True)
                if bat % sample_interval == 0:
                    self.plot_images(save2file=True, epoch=epoch, batch=bat)
            print(
                'd_loss:%3.3f, g_loss: %3.3f' %
                (d_loss[0], g_loss), flush=True)
            self.save_weights()
            self.save_weights(
                G_weights_name='epoch_%d_G_weights.h5' %
                epoch,
                D_weights_name='epoch_%d_D_weights.h5' %
                epoch)

    def save_weights(self, G_weights_name='G_weights.h5',
                     D_weights_name='D_weights.h5'):
        try:
            os.mkdir('model')
        except BaseException:
            pass
        self.G.save_weights('model/' + G_weights_name)
        self.D.save_weights('model/' + D_weights_name)

    def load_weights(self, G_weights_name='G_weights.h5',
                     D_weights_name='D_weights.h5'):
        self.G.load_weights('model/' + G_weights_name)
        self.D.load_weights('model/' + D_weights_name)

    def plot_images(self, save2file=False, samples=16, epoch=0, batch=0):
        ''' Plot and generated images '''
        if not os.path.exists("./images"):
            os.makedirs("./images")
        filename = "./images/epoch_%d_batch_%d.png" % (epoch, batch)
        noise = np.random.normal(0, 1, (samples, 100))

        images = self.G.predict(noise)

        plt.figure(figsize=(10, 10))

        for i in range(images.shape[0]):
            plt.subplot(4, 4, i + 1)
            image = images[i, :, :, :]
            plt.imshow(image)
            plt.axis('off')
        plt.tight_layout()

        if save2file:
            plt.savefig(filename)
            plt.close('all')
        else:
            plt.show()

# class WGAN_GP_Optimizer(Optimizer):


class WGAN_GP(WGAN):
    def __init__(self, width=96, height=96, channels=3, lr=1e-3,
                 clipvalue=.2, clipnorm=.2, alphaa1=1, alpha2=0.5):
        self.shape = (width, height, channels)
        # original:lr=0.001, rho=0.9, epsilon=None, decay=0.0
        self.Doptimizer = RMSprop(
            lr=lr, rho=0.9, epsilon=None, decay=0.0, clipvalue=clipvalue, clipnorm=clipnorm)
        self.optimizer = RMSprop(lr=lr, rho=0.9, epsilon=None, decay=0.0)

        self.G = self.__generator()
        self.D = self.__discriminator()

        self.G.compile(loss='binary_crossentropy', optimizer=self.optimizer)
        self.D.compile(loss=lambda y_true, y_pred: -K.mean(y_pred * (y_true - 0.5) * 2), optimizer=self.Doptimizer,
                       metrics=['accuracy'])

        self.stacked_GD = self.__stacked_GD()

        self.stacked_GD.compile(
            loss=lambda y_true, y_pred: -K.mean(y_pred), optimizer=self.optimizer)


if __name__ == '__main__':
    wgan = WGAN()
    wgan.train(epochs=25, batch=80, load=True)
