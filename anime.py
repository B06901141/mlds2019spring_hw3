import argparse
import itertools
import os

import tensorboardX as tb
import torch
import torch.distributions as P
import torch.nn.functional as F
import torch.utils.data as data
import torchvision.transforms as T
import torchvision.utils as tv_utils
import tqdm
from torch import autograd, cuda, nn, optim

import dataUtils as dutils
import models


def train_one_epoch(gen, dis, gen_opt, dis_opt, dataloader, latent_dim,
                    ratio, grad_penalty, progbar, count, device, saveas=None):
    for i, x in enumerate(dataloader):
        progbar.update(len(x))
        x = x.to(device)
        scalar_real = scalar_fake = 0
        for _ in range(ratio):
            noise = torch.randn(len(x), latent_dim, device=device)
            real = dutils.GradReverse(dis(x))
            with torch.no_grad():
                gtd = gen(noise)
            fake = dis(gtd)

            value = real.mean() + fake.mean()
            grads = autograd.grad(value, dis.parameters(), create_graph=True)
            grad_norm = torch.cat([g.view(-1) for g in grads], dim=0).norm(2)
            grad_norm = ((grad_norm - 1)**2) * grad_penalty
            total = value + grad_norm

            dis.zero_grad()
            total.backward()
            dis_opt.step()

            scalar_real += real.mean().item()
            scalar_fake += fake.mean().item()

        noise = torch.randn(len(x), latent_dim, device=device)
        gtd = gen(noise)
        creation = dutils.GradReverse(gtd)
        fool = dis(creation)
        value = fool.mean()

        gen.zero_grad()
        value.backward()
        gen_opt.step()

        scalar_fool = fool.mean().item()

        scalar_real /= ratio
        scalar_fake /= ratio

        count += 1
        writer.add_scalar(tag="real",
                          scalar_value=scalar_real,
                          global_step=int(count))
        writer.add_scalar(tag="fake",
                          scalar_value=scalar_fake,
                          global_step=int(count))
        writer.add_scalar(tag="fool",
                          scalar_value=scalar_fool,
                          global_step=int(count))

        if saveas:
            create_image(gen, latent_dim,
                         os.path.join(f"{saveas}", f"{int(count):05d}.png"), device)

    progbar.close()


def create_image(generator, latent_dim, savepath, device, *args, **kwargs):
    starting_point = P.Uniform(low=-1, high=1).sample([latent_dim])
    step_x = P.Normal(loc=0, scale=.1).sample([latent_dim])
    step_y = P.Normal(loc=0, scale=.1).sample([latent_dim])
    input_tensor = torch.stack([starting_point + step_x * x + step_y * y for x,
                                y in itertools.product(range(10), range(10))], dim=0).to(device)
    tv_utils.save_image(
        tensor=generator(input_tensor).cpu().reshape(100, 3, 64, 64), filename=savepath, nrow=10)


def train(epochs, dataloader, max_norm, latent_dim, load_trained,
          ratio, grad_lambda, learning_rate, device, writer, saveas=None):
    count = dutils.Counter()
    if load_trained:
        try:
            gen_state = torch.load("Generator.pt",
                                   map_location=device)
            dis_state = torch.load("Discriminator.pt",
                                   map_location=device)
            gen = models.Generator(latent_dim).to(device)
            gen.load_state_dict(gen_state)
            dis = models.Discriminator().to(device)
            dis.load_state_dict(dis_state)
        except FileNotFoundError as fnfe:
            print(repr(fnfe))
            gen = models.Generator(latent_dim)
            dis = models.Discriminator()
    else:
        gen = models.Generator(latent_dim)
        dis = models.Discriminator()

    if cuda.device_count() > 1:
        gen = nn.DataParallel(gen)
        dis = nn.DataParallel(dis)

    gen = gen.to(device)
    dis = dis.to(device)
    gen_opt = optim.RMSprop(gen.parameters(), lr=learning_rate)
    dis_opt = optim.RMSprop(dis.parameters(), lr=learning_rate)

    for i_epoch in range(1, 1 + epochs):
        print(f"epoch {i_epoch:03d}", end=' ')
        train_one_epoch(gen, dis, gen_opt, dis_opt, dataloader,
                        latent_dim, ratio, grad_lambda,
                        tqdm.tqdm(total=len(dataloader.dataset)),
                        count, device, saveas=saveas)

    torch.save(obj=gen.cpu().state_dict(), f="Generator.pt")
    torch.save(obj=dis.cpu().state_dict(), f="Discriminator.pt")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-dv', "--device", type=str, default="cuda")
    parser.add_argument('-ld', "--latent-dim", type=int, default=100)
    parser.add_argument('-gp', "--gradient-penalty", type=float, default=1)
    parser.add_argument('-dp', "--dir-path", type=str,
                        default="WaterStone/faces")
    parser.add_argument('-sa', "--saveas", type=str)
    parser.add_argument('-ep', "--epochs", type=int, default=300)
    parser.add_argument('-lr', "--learning-rate", type=float, default=1e-2)
    parser.add_argument('-bt', "--batch-size", type=int, default=2048)
    parser.add_argument('-mn', "--max-norm", type=float, default=1)
    parser.add_argument('-lt', "--load-trained", action="store_true")
    parser.add_argument('-tr', "--train", action="store_true")
    parser.add_argument('-ro', "--ratio", type=int, default=5)
    parser.add_argument('-lg', "--log-dir", type=str, default="wgan-anime")
    flags = parser.parse_args()

    saveas = flags.saveas
    if saveas:
        os.makedirs(saveas, exist_ok=True)

    device = flags.device if cuda.is_available() else "cpu"

    writer = tb.SummaryWriter(log_dir=flags.log_dir)

    dataset = dutils.FacesDataset(folder=flags.dir_path)
    dataloader = data.DataLoader(dataset, batch_size=flags.batch_size)

    if flags.train:
        train(flags.epochs, dataloader, flags.max_norm,
              flags.latent_dim, flags.load_trained,
              flags.ratio, flags.gradient_penalty, flags.learning_rate,
              device, writer, saveas=flags.saveas)

    else:
        try:
            gen_state = torch.load("Generator.pt",
                                   map_location=device)
            gen = models.Generator(flags.latent_dim).to(device)
            gen.load_state_dict(gen_state)
            create_image(gen, flags.latent_dim,
                         savepath=flags.saveas, device=device)
        except FileNotFoundError as fnfe:
            print(repr(fnfe))
            print("Cannot generate useful images!")
